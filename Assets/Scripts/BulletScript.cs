using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float Speed = 20f;
    public float MovementSmoothing = .05f;
    private Vector3 _velocity = Vector3.zero;
    public int direction = 1;
    public bool Shot = false;

    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        if (Shot)
        {
            Vector2 targetVel = new Vector2(Speed * direction, 0);
            _rigidbody.velocity = Vector3.SmoothDamp(_rigidbody.velocity, targetVel, ref _velocity, MovementSmoothing);
        }
    }

    public void PlayerShot()
    {
        if (direction != 1)
            Flip();
        Shot = true;
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
