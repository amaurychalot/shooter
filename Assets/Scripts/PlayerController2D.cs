using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class PlayerController2D : MonoBehaviour
{
    public Animator animator;
    public BulletScript BulletPrefab;
    public GameObject ShootingExplosion;

    public float RunSpeed = 40f;

    private float _horizontalMove = 0f;
    public float MovementSmoothing = .05f;
    private Vector3 _velocity = Vector3.zero;
    private bool _facingRight = true;
    
    private Rigidbody2D _rigidbody;
    
    private bool _jump = false;
    public float JumpForce = 400f;
    private bool _grounded = true;
    
    private bool _shooting = false;
    private bool _shootingRight = true;
    private float _timeNextShot = 0f;
    private float _timeBetweenShots1 = 0.20f;
    private float _timeBetweenShots2 = 0.16f;
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void MakeExplosion()
    {
        GameObject explosion;
        if (_shootingRight)
            explosion = Instantiate(ShootingExplosion, 
                new Vector3(transform.position.x + (_facingRight ? 1.7f : -1.7f), transform.position.y - 0.9f,
                    transform.position.z - 0.1f)
                , Quaternion.identity);
        else
            explosion = Instantiate(ShootingExplosion, 
                new Vector3(transform.position.x + (_facingRight ? 2.5f : -2.5f), transform.position.y - 0.7f,
                    transform.position.z - 0.1f)
                , Quaternion.identity);
        
        Vector3 expScale = explosion.transform.localScale;
        expScale.x *= _facingRight ? 1 : -1;
        explosion.transform.localScale = expScale;
    }
    
    private void Shooting()
    {
        if (Input.GetMouseButton(0))
        {
            if (_timeNextShot <= Time.time)
            {
                _timeNextShot = Time.time + (_shootingRight ? _timeBetweenShots1 : _timeBetweenShots2);
                BulletScript bullet;
                if (_shootingRight)
                {
                    bullet = Instantiate(BulletPrefab,
                        new Vector3(transform.position.x + (_facingRight ? 1.5f : -1.5f), transform.position.y - 1f,
                            transform.position.z), Quaternion.identity);
                }
                else
                {
                    bullet = Instantiate(BulletPrefab,
                        new Vector3(transform.position.x + (_facingRight ? 1.7f : -1.7f), transform.position.y - 0.8f,
                            transform.position.z), Quaternion.identity);
                }
                bullet.direction = _facingRight ? 1 : -1;
                bullet.PlayerShot();
                MakeExplosion();
                _shootingRight = !_shootingRight;
            }
        }

        else
        {
            _shootingRight = true;
        }
        
        _shooting = Input.GetMouseButton(0);
    }
    private void Update()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal") * RunSpeed;
        
        //Call animator Run
        animator.SetFloat("Speed", Mathf.Abs(_horizontalMove));
        animator.SetBool("Falling", _rigidbody.velocity.y < 0);
        
        Shooting();

        if (Input.GetButtonDown("Jump"))
        {
            _jump = true;
            //Call animator Jump
        }
        
        animator.SetBool("Jump", _jump || !_grounded);
        animator.SetBool("Shoot", _shooting);
        
        Move(_horizontalMove, _jump);
        _jump = false;
    }

    public void Move(float move, bool jump)
    {
        Vector2 targetVelocity = new Vector2(move * 10f, _rigidbody.velocity.y);
        _rigidbody.velocity = Vector3.SmoothDamp(_rigidbody.velocity, targetVelocity, ref _velocity, MovementSmoothing);

        if (move > 0 && !_facingRight)
        {
            Flip();
        }
        else if (move < 0 && _facingRight)
        {
            Flip();
        }
        
        if (_grounded && jump)
        {
            _grounded = false;
            _rigidbody.AddForce(new Vector2(0f, JumpForce));
        }
    }

    void Flip()
    {
        _facingRight = !_facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
        {
            _grounded = true;
        }
    }
}
